package com.suparna.customerservice.controller;

import com.suparna.customerservice.model.Customer;
import com.suparna.customerservice.repository.CustomerRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RefreshScope
@Validated
@RestController
@RequestMapping("/api")
@Tag(name = "customer", description = "The Customer API")
public class CustomerController {

    protected static Logger logger = LoggerFactory.getLogger(CustomerController.class.getName());

    @Autowired
    CustomerRepository customerRepository;

    @Operation(summary = "Add a new customer", description = "", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Customer added",
                    content = @Content(schema = @Schema(implementation = Customer.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Customer already exists") })
    @RequestMapping(value="/customers", method = RequestMethod.POST)
    public ResponseEntity<Customer> createCustomer(@Parameter(description="Customer to add. Cannot null or empty.",
            required=true, schema=@Schema(implementation = Customer.class))
                                                   @Valid @RequestBody Customer customer){
        try{
            return new ResponseEntity<>(customerRepository.save(customer), HttpStatus.CREATED);
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
//    @Autowired
//    FlightSearchClient flightSearcgFeignClient;
//
//    @GetMapping("/customer/getFlightDetails/{flightId}")
//    ResponseEntity<FlightSearch> getBookingDetailsByCustomerIDAndFlightID(@PathVariable Long flightId) {
//
//        return flightSearcgFeignClient.getFlightByFlightId(flightId);
//    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable Long id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (customer.isPresent()) {
            return new ResponseEntity<>(customer.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        try {
            List<Customer> list = customerRepository.findAll();

            if (list.isEmpty() || list.size() == 0) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/customers")
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        try {
            return new ResponseEntity<>(customerRepository.save(customer), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
