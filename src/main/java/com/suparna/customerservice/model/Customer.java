package com.suparna.customerservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="customer")
@Setter
@Getter
@ToString
public class Customer implements Serializable {

    private static final long serialVersionUID = 4048798961366546485L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    @Schema(description = "Unique identifier of the Customer.",
//            example = "1")
    private Long customerid;

    @JsonProperty("customerName")
    @NotEmpty(message = "Customer Name Is Required")
    @Size(max = 100)
    private String customerName;

    @JsonProperty("bookingId")
    private String bookingId;

    @CreationTimestamp
    @Column(name="created_at", nullable=false, updatable=false)
    private Date createdAt;

    @UpdateTimestamp
    @Column(name="updated_at")
    private Date updatedAt;
}
